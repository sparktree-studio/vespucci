# Vespucci Mapping Bot

Generates a list of the categories and channels on the server, with the topic of each channel, so that users can more easily review the options available to them on your server. 😁

## Commands

### Map Review

`/map_review`: List channels that don't have a topic set (in case you want to fix them before generating the server map).

### Map Server

`/map_server`: List server categories and channels, with their topics.

### Map Category

`/map_category`: List channels in a specific category (for adding to private categories of a server, for example).

## Development

After setting up the bot account and credentials (not going into that now), put them in your environment (however you like):

```shell
# .env if you want to use the dotenv lib
# DISCORD APP CREDENTIALS
# =======================================
# ClientID from the Oauth2 settings page
# https://discord.com/developers/applications/<yourapp>/oauth2/general
CLIENT_ID=APPLICATION_ID=""
# Token from
# https://discord.com/developers/applications/<yourapp>/bot
TOKEN=""
# Turn on Discord Dev tools to copy the server id (guildID in the API)
# for the test server.
# used to register commands, but should be converted to part of the
# add server process
GUILD_ID=""
```
